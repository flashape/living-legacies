//Require the express package and use express.Router()
const express = require('express');
const router = express.Router();


function chainError(err) {
    return Promise.reject(err);
}

//GET HTTP method to /post
router.get('/',(req,res) => {
    res.send("GET POST");
    //res.send(res.app.locals.firebaseAdmin)

});

//POST HTTP method to /post

router.post('/', (req,res,next) => {
    res.send("POST");

});

//DELETE HTTP method to /post. Here, we pass in a params which is the object id.
router.delete('/:id', (req,res,next)=> {
    res.send("DELETE");

})


router.post('/auth', (request, response)=>{
    console.time('authpost')

    console.log('/authpost...')

    let postId = request.body.postId;
    let userId = request.body.userId;
    let postUserId = request.body.postUserId;

    let db = request.app.locals.firebaseAdmin.firestore();

    console.log("authorizing post : ", request.body)

    let postDoc = db.doc('users/'+postUserId+'/posts/'+postId);
    console.time('gettingPostDoc');

    console.log('getting postDoc...')

    postDoc.get().then(documentSnapshot => {
        console.timeEnd('gettingPostDoc');

        console.log('got documentSnapshot : ', documentSnapshot)
        console.log('got documentSnapshot exists: ', documentSnapshot.exists)
        if (documentSnapshot.exists) {
            console.log('post document retrieved successfully.');
            let postOwnerId = documentSnapshot.get('userId');
            console.log('postOwnerId : ', postOwnerId);
            if(postOwnerId === userId){
                console.log('is post owner, returning isAuthorized: true')
                response.json({isAuthorized:true, isOwner: true})
            }else{
                console.log("is not post owner, checking invited users...")
                let invitedUser = documentSnapshot.get(`invitedUsers.${userId}`);
                console.log("invitedUser : ", invitedUser);

                if(invitedUser){
                    response.json({isAuthorized:true, isOwner: false, isInvitedUser: true})

                }else{
                    response.json({isAuthorized:false, isOwner: false, isInvitedUser: false})
                }

            }

        }else{
            console.log("user does not exist...")
            response.json({isAuthorized:false, isOwner: false, isInvitedUser: false})
        }
    })
        .catch(error =>{
            console.log('Error : ', error)
            response.json({isAuthorized:false, isOwner: false, isInvitedUser: false})
        })
    console.timeEnd('authpost')

})


router.post('/auth-editor', (request, response)=>{
    console.log('/auth-editor...')
    console.time('auth-editor')
    let postId = request.body.postId;
    let userId = request.body.userId;
    let postUserId = request.body.postUserId;
    console.time('initializingFiresstore');
    let db = request.app.locals.firebaseAdmin.firestore();
    console.timeEnd('initializingFiresstore');

    console.log("authorizing post : ", request.body)
    console.time('gettingPostDoc');

    let postDoc = db.doc('users/'+postUserId+'/posts/'+postId);
    // console.log('getting postDoc...')
    postDoc.get().then(documentSnapshot => {
        console.timeEnd('gettingPostDoc');

        console.log('got documentSnapshot : ', documentSnapshot)
        console.log('got documentSnapshot exists: ', documentSnapshot.exists)
        if (documentSnapshot.exists) {
            //console.log('post document retrieved successfully.');
            let postOwnerId = documentSnapshot.get('userId');
            //console.log('postOwnerId : ', postOwnerId);
            if(postOwnerId === userId){
                //console.log('is post owner, returning isAuthorized: true')
                response.json({isAuthorized:true, isOwner: true})
            }else{
                response.json({isAuthorized:false, isOwner: false})

            }

        }else{
            response.json({isAuthorized:false, isOwner: false})
        }
    })
        .catch(error =>{
            console.log('Error : ', error)
        })
    console.timeEnd('auth-editor')

})



router.post('/share', (request, response)=>{
    console.log('share-post body: ', request.body);

    let emails = request.body.emails;
    let message = request.body.message;
    let senderUid = request.body.senderId;
    let postId = request.body.postId;

    let senderEmail = '';
    let admin = request.app.locals.firebaseAdmin;
    console.log('emails : ', emails);
    console.log('message : ', message);

    // first add the invitation to the db
    let db = admin.firestore();

    let user;

    admin.auth().getUser(senderUid)
        .then((userRecord) => {
            // See the UserRecord reference doc for the contents of userRecord.
            console.log("Successfully fetched user data:", userRecord.toJSON());
            senderEmail = userRecord.email;
            return Promise.resolve(userRecord)
        }, chainError)




        .then(userRecord=>{
            user = userRecord;
            console.log("got user record : ", userRecord)
            let invitationCollection  = db.collection('invitations');

            return invitationCollection.add({
                emails: emails,
                message: message,
                postId: postId,
                senderId: senderUid
            })



        }, chainError)

        .then(invitationDoc=>{
            console.log("got invitationDoc : ", invitationDoc)
            // let data: any = {};
            // data.invitations = {};
            // data.invitations[invitationDoc.id] = true;

            let key = `invitations.${invitationDoc.id}`;
            let data = {};
            data[key] = true;


            db.doc('users/'+user.uid).update(data).catch()

            return invitationDoc;
        }, chainError)


        .then(invitationDoc=>{
            // // Begin a new batch
            // var batch = db.batch();
            console.log("got invitationDoc : ", invitationDoc)
            console.log("setting emails to invitation...")
            let emailsCollection = invitationDoc.collection('emails');
            console.log("emails : ", emails);
            // Set each document, as part of the batch
            emails.forEach(email => {
                console.log("adding email : ", email)
                let emailInviteDoc = emailsCollection.doc(email).set({
                    email: email,
                    invitationId: invitationDoc.id,
                    visited: false,
                    created: admin.database.ServerValue.TIMESTAMP,
                    visitTimestamp: null,
                    postId: postId
                });
                //batch.set(ref, t);
            })

            // send the emails


            let api_key = 'key-a46618d207943cea4aae2125b407f889';
            let domain = 'sandboxd61a6c1f87e34e7a80e0b8b6f61445a7.mailgun.org';
            let mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

            let siteUrl = "http://localhost:5200/i/"+invitationDoc.id+'-';

            emails.forEach(email => {

                let trimmedEmail = email.trim();

                if(trimmedEmail.length === 0){
                    return;
                }

                let encodedEmail =  Buffer.from(trimmedEmail).toString('base64');

                let data = {
                    from: 'Living Legacies <no-reply@samples.mailgun.org>',
                    to: trimmedEmail,
                    subject: 'View a memory on LivingLegacies.com!',
                    text: `${senderEmail} invited you to view their memory at LivingLegacies.com!

Click here to view : ${siteUrl}${encodedEmail}`
                };


                mailgun.messages().send(data, (error, body) => {
                    console.log(body);
                    // response.json({success:true, emails:emails, message:message, sendResult:body});
                });
            })


            response.json({success:true})

        }, chainError)

        .catch(error=>{
            console.log('error : ', error)
        })



    //Buffer.from("SGVsbG8gV29ybGQ=", 'base64').toString('ascii'))
})




module.exports = router;
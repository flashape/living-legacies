//Require the express package and use express.Router()
const express = require('express');
const router = express.Router();


function chainError(err) {
    return Promise.reject(err);
}

//GET HTTP method to /post
router.get('/',(req,res) => {
    res.send("GET ACCOUNT");
    //res.send(res.app.locals.firebaseAdmin)

});

router.post('/subscription', (request, response) =>{
    console.log('/subscription...')
    let token = request.body.token;
    let planId = request.body.planId;
    let stripe = request.app.locals.stripe;

    stripe.customers.list({limit:1, email:token.email}).
        then(result=>{
            if(result.data.length === 0){
                // create stripe customer
                return stripe.customers.create ({
                    email: token.email,
                    source:token.id
                })
            }else{
                throw new Error('Customer already exists!')
            }

        }, chainError)



        .then(customer =>{
            console.log("created customer : ", customer);

            // subscribe customer to plan

            return stripe.subscriptions.create({
                customer: customer.id,
                items: [{plan: 'plan_CMNLIqYsNIZOq7'}],
            });
        }, chainError)



        .then(subscription=>{
            console.log('created subscription : ', subscription);
            response.json({success:true, subscription:subscription});
        }, chainError)



        .catch(error =>{
            console.log("error creating customer : ", error)
            let err;

            if(!error.hasOwnProperty('statusCode')){
                err = {};
                err['statusCode'] = 400;
                err['message'] = error.message;
            }else {
                err = error;
            }
            response.status(err.statusCode).json(err);
        })


    // save stripe customer id to db
})


router.post('/membership', (request, response) =>{
    console.log('/membership...')

    let token = request.body.token;
    let sku = request.body.sku;
    let stripe = request.app.locals.stripe;

    stripe.customers.list({limit:1, email:token.email}).

    // get or create customer...
    then(result=>{
        if(result.data.length === 0){
            // create stripe customer
            return stripe.customers.create ({
                email: token.email,
                source:token
            })
        }else{
            console.log('Found existing customer : ', result.data[0])
            return Promise.resolve(result.data[0])
        }

    }, chainError)


    // now that we have the customer, create the order
        .then(customer => {
            console.log("received customer, now creating order for customer : ", customer);

            return stripe.orders.create({
                currency: 'usd',
                items: [
                    {
                        type: 'sku',
                        parent: sku
                    }
                ],
                customer: customer.id
            })

        }, chainError)


        // now that we have the order for the customer, pay the order
        .then(order=>{
            console.log('created order, now paying : ', order)
            console.log('using token  : ', token)
            return stripe.orders.pay(order.id, {
                source: token.id,
            })
        }, chainError)


        // now that we have paid the order mark it as fullfilled
        .then(order => {
            console.log('paid order now fullfilling: ', order)
            return stripe.orders.update(order.id, {
                status: 'fulfilled'
            });
        }, chainError)


        // we're done
        .then(order=>{
            console.log('Membership created : ', order)
            response.json({success:true, order:order})
        }, chainError)



        .catch(error =>{
            console.log("error purchasing membership : ", error)
            let err;

            if(!error.hasOwnProperty('statusCode')){
                err = {};
                err['statusCode'] = 400;
                err['message'] = error.message;
            }else {
                err = error;
            }
            response.status(err.statusCode).json(err);
        })


    // save stripe ORDER id to db
})


module.exports = router;
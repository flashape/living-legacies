// We’ll declare all our dependencies here
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const post = require('./controllers/post');
const account = require('./controllers/account');
const auth = require('./controllers/auth');
const admin = require('firebase-admin')
const helmet = require('helmet');

const stripeKey = 'sk_test_9f5V4lK0UFjb8X3ZAPSiJ1wX';
const stripe = require('stripe')(stripeKey);

//Initialize our app variable
const app = express();

//Declaring Port
const port = process.env.PORT || 5000;

// const stripeKey = 'sk_test_9f5V4lK0UFjb8X3ZAPSiJ1wX';
// const stripe = new Stripe(stripeKey);
app.locals.stripe = stripe;

//Middleware for CORS
app.use(cors());
app.use(helmet());

//Middleware for bodyparsing using both json and urlencoding
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());


admin.initializeApp({
    credential: admin.credential.cert({
        "projectId": "living-legacy-ab939",
        "privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDW6KsKFxsEWrzn\nrSk5tMmYE1iRONr4/GvhAFqcn13KcHY8QpSRplZHXw3O4mydEfFpdTmgNcF+M4D7\nflXHMV+jUEzBLF+nC7YZqFBj0MnIukNBhMA2bFuladAuCkWZ2bH/zgbd/6gF6Xfp\nWqoIlt/WHN1/DKdc/H+wQxo0xgiPVSqTmFI3RfWDOEAhLU1j8N9nbwCWcnI9zdmA\nXZO4trJbT1Oazl+eNbJDjvazYGpu/Xs1k7D55JaoL5mkuP3gudge3HuBrZjJbvur\nfwL5t/oC+GVxFWbliEMH2Ag+y3r3iAHnA5pB7AOSg7cwd/JZIAZOtY87YnqA7sEq\ngRGkus2vAgMBAAECggEALvTcdlJxGUT4NIi90rmBOMS1W9m34qJIsfthUQpzNEnZ\nU7Zafu6AuIx42GTZxYIHpPqzkiFcu0Xmn0zN0j2j/2uKscSPy9B/w+BmXEDg7gZg\nrp6iXuHTLnPNxFmDNqf9v/5Uusni/ROzmx2vvDu3AbIB5YIuyV70SV+ICNLiSOq0\nnUhtu2v21s+xwOComhv+DlinX+c1d+X/xg+GibUBT8w+DPyr+4ho2XrEilsWhfSg\n1oB+xciJdHN+kT+dLYO2dAHBU1Z2G9AQmVXC7LVSLPTcolW2v16RGU/Zf9MJ/wma\nojJlelNH0Di8z002KnywHju8zCIbE+b7n8J8f1Sb+QKBgQD8+J/3qNj6Rtri/xaA\nhaWPIlBy68qnBCO4oBq00Vfs+gWk5PdA/HNcPAhpuL9HfcRQvWfBiv5DNVLvWNm9\nP9Hxh7SaKU2gOFu7ERM57GQP+XieH1J2JOX3IvCesZfa5wtSh6svx6z2+Stl54wp\n7VGUTewLTGML/Y5plA2zrYqyRwKBgQDZe2EigOE8S/0+OvDPe43J9SO+L4xIRymu\n0KqfTv9kntJsQ3/XVz8pD1qbXk3q+a0/5wjKrjna+I+4HIvoaJxixiujN+4+KOYM\n1MR42mhcBT5afDlKGpSBHkiN5NQgLL2xvqs47zTmsHN22qszQTu2SdaOfiDnaT85\nIzoOMIUVWQKBgQCwapLknBP1NkSf2VVP++QhlcSU4GtLXk+rCbyWLQgsc5BMFoF4\nCJZK1HLph7Wdnc+ynuQkzVHUfPOBh20R1dgLoKAKgpNoEOWn55iI5JpDqa68Vwui\n6MiXOP6lLSTBvC7hN0vi3lV5RvtE7d2Jv78MterPVk4cpo8SgfWsQHJhMQKBgQCA\ncAz9g3+BTJ9SkRyGbEFgdBWXzBzc0qobQ8CANPc/RUygA30Mf8odRaQM/bSmgK2A\nf8EBccNZPlIiOTmnF6DD9KTwz6czrpOuIDmn5dScA7y/U33B9dCeMHRI2EsudT3S\nKviTtzFWy/5kwt84dd392b2MiZCFGfLsm5E9sAnogQKBgQCouLXBkNkWr7YlauWs\nLY5afu4eGOry6S0WXsjlz3cdpn3h1efxh/gzivHzNi0z2nmiE8Cn346dKJVaokAz\nPDEyTubNcbkZdq0G0vLCdUMZDPicyQyeuqcgyPTbmSFiX0l4PZkylgmye2rlvXgA\neGzkOjN/Jhh9DGp04xR4urJQVw==\n-----END PRIVATE KEY-----\n",
        "clientEmail": "firebase-adminsdk-rzk7q@living-legacy-ab939.iam.gserviceaccount.com"
    }),
    databaseURL: 'https://living-legacy-ab939.firebaseio.com'
});

app.locals.firebaseAdmin = admin;


app.use('/v1/post', post);
app.use('/v1/account', account);

/*express.static is a built in middleware function to serve static files.
 We are telling express server public folder is the place to look for the static files
*/
//app.use(express.static(path.join(__dirname, 'public')));

//Listen to port 3000
app.listen(port,  () => {
    console.log(`Starting the server at port ${port}`);
})


// app.listen(port, () => {
//     console.log(`Starting the server at port ${port}`);
// });

app.get('/', (req,res) => {
    res.send("Invalid page");
})